<?php
/**
 * Implementation of hook_ulink_user()
 */
function ulink_ulink_user($link, $user = NULL){
  return _ulink_ulink_xxx('user', $link, $user);
}
/**
 * Implementation of hook_ulink_user_settings()
 */
function ulink_ulink_user_settings() {
   return _ulink_ulink_settings_builder('user');
}
/**
 * Implementation of hook_ulink_user_info()
 */
function ulink_ulink_user_info() {
  return t(' Three modes of rendering are possible. Hardcoded rendering for novice users and PHPcode for advanced users. In the macro mode TOKENS can be used. Use configurations to individually configure them to match the requirement.');
}
/*
 * support functions to provide extra settings pages.
 */
function _ulink_user_settings_1() {
  return system_settings_form(_ulink_xxx_settings_1('user'));
} 
function _ulink_user_settings_2() {
  return system_settings_form(_ulink_xxx_settings_2('user'));
}
function _ulink_user_settings_3() {
 return  system_settings_form(_ulink_xxx_settings_3('user'));
}