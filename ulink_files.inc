<?php
/**
 * Implementation of hook_ulink_file().
 */
function ulink_ulink_files($link, $files=NULL){
  return _ulink_ulink_xxx('files', $link, $files);
}
/**
 * Implementation of hook_ulink_file_settings().
 */
function ulink_ulink_files_settings() {
  return _ulink_ulink_settings_builder('files');
}
/**
 * Implementation of hook_ulink_files_info().
 */
function ulink_ulink_files_info() {
  return t(' Three modes of rendering are possible. Hardcoded rendering for novice users and PHPcode for advanced users. In the macro mode TOKENS can be used. Use configurations to individually configure them to match the requirement.');
}
/*
 * support functions
 */
function _ulink_files_settings_1() {
  return system_settings_form(_ulink_xxx_settings_1('files'));
} 
function _ulink_files_settings_2() {  
  return system_settings_form(_ulink_xxx_settings_2('files'));
}
function _ulink_files_settings_3() {
 return  system_settings_form(_ulink_xxx_settings_3('files'));
}
