********************************************************************
                     D R U P A L    M O D U L E
										 an extension for uLink module
********************************************************************
Name: uAuto module
Author: Gartheeban Ganeshapillai 
        AKA garthee at drupal <garthee at gmail dot com>
Dependencies: uLink.module, filter.module

********************************************************************
IMPORTANT

Users are encouraged to read the README.txt of uLink module

********************************************************************
DESCRIPTION:

The uAuto module does auto-completion of links by providing the user
with possible links for the text selected or being typed. For example,
when user enters [l|use, he will be provided with the possible links
that could be connected with the key �use�. Alternatively, he could
select the complete word by double clicking it or highlighting it,
and he will be provided with the possible candidates.

The module uses the protocol defined by uLink module and generates
the link in the format:
 [l-macroname|link|text]
 or 
 [l|link|text|attribute]
 
The module provides an excellent, configurable and extensive search
(can be optionally enabled) which can be extended (by implementing
the hook under circumstances like connecting two peer sites or
supporting external link auto completion).

uAuto_dirsearch module is written as a seperate module to explain
how search funtionalities can be extended. uAuto_dirsearch lists 
the files inside a directory which lies inside 'files' so that
user can pick up a file to reference to.

Settings for the module
------------------------------------------------------------------
Available under 
Admin > Settings > ulink > General settings > uauto - settings
(admin/settings/ulink)

Allow root-user to be referred :
		Some sites may want to prevent root user account from referring to
		in such case this could be clicked off.
		
kSearch:
		kSearch is triggered when user enters
		[l|
		or
		[l-macroname|
		
		Timeout:
				The time in seconds to be lapsed after last keypress before
				initiation AJAX search for the text user entered. By setting
				enough time will allow user to type a meaningful word before
				firing ajax call.
iSearch
		iSearch is triggered when user selects a word by highlighting it or 
		double clicking it.
		
		Extensive node search:
				By enabling this, users can benefit from Drupals extensive
				search at the expense of longer time. However, results are cached,
				therefore subsequent searches would benefit
				
Caching
-------------------------------------------------------------------
All results are locally (at client side) cached and used when called
repeatedly. However it can be cleared by pressing 'clear cache' button.
In addition by ticking Remember forever, user selections are sent to
the server to make subsequent searches, especially those involving extensive
node searches quicker.

*** Important suggestion ***
	Having this enabled for sometime will move popular results to the cache
	and then extensive search can be turned off. users will still get the
	results from cache. You should tick Remember All to cache the results at
  server side.
		

External References
--------------------------------------------------------------------
[1] Implementing uLink hooks 
Developer_documentation_ulink.txt
[2] Readme for ulink module
Readme.txt

********************************************************************
SYSTEM REQUIREMENTS

Drupal 5.x 
PHP 5.0.0 or greater
Filter.module installed

********************************************************************

CREDITS:

 - Thanks to Google for sponsoring this project through GSOC 2007
 - Thanks to Drupal community, Daniel DeGeest and Kaustubh Srikanth for seleting
	 this module for GSOC


********************************************************************
BUGS AND SUGGESTIONS

Please report all bug reports at:
http://drupal.org/project/issues/ulink

Contact me at:
garthee at theebgar dot net


Demos at:
http://www.project.theebgar.net
http://mwt.argz.com/ulink/tests



