
//global variables needed for operations between multiple functions
var mode = 0;
var lenOfId = 0;
var link_text = null;

var body = null; //id of text area
var macro = null;// var macroList;
var kTimer; //t0 delay ajax search to allow user to complete the word
var cacheList = new Object();// search cache lists
var state = false;//state of popup layers
var applyCache = false;

// to keep track of user selection
var selectionStart;
var selectionEnd;
var userSelection;

//Global killswitch
if (Drupal.jsEnabled) {
  $(document).ready(function() {

	  // to turn off kSearch and iSearch momentarily
	  var tempa = uauto.k_enable? 'checked' : 'disabled';
		var tempb = uauto.i_enable? '' : 'disabled';
    uauto.i_enable = false;
    
    $("body").prepend('<div class="uauto-widget" id="uauto-widget"></div>');    
		$("#edit-body").before('<div id="uauto-display"><i>UAUTO Auto completion</i><input name="ksearch" id="uauto-ksearch-chk" type="checkbox" value="Enable Ksearch"' + tempa +'> Enable Ksearch<input name="isearch" id="uauto-isearch-chk" type="checkbox" value="Enable Isearch" ' + tempb +'> Enable Isearch<input name="clear" type="button" value="Clear cache" id="uauto-k-clear" style=" width:115px;"><input name="Cache" id="uauto-i-cache" type="checkbox" value="Cache"> Remember forever</div>');
    // clearing cache - to save memory
    $('#uauto-k-clear').click( function() {
      if (confirm("Do you want to clear cache? ")) {
        cacheList = new Object();
      }
    });
    $("input[@type=checkbox]").click( function() {	
			uauto.k_enable = (this.id=="uauto-ksearch-chk") ? this.checked: uauto.k_enable;
			uauto.i_enable = (this.id=="uauto-isearch-chk") ? this.checked: uauto.i_enable;
      applyCache = (this.id=="uauto-i-cache") ? this.checked: applyCache;      
		});	    
    //keysearch root function
    $("#edit-body").keypress(function(e) {   		
      body = this;
      
      //escape k_search
      if (uauto.k_enable == 0)
        return true;
				
      var keyCode = e.charCode ? e.charCode : e.keyCode ? e.keyCode : e.which ? e.which : void 0;

      switch (mode) {
      case 0:			
			  // to close unneccessary windows
			  if (state) {
					state = false;
					$('#uauto-i').hide("fast");
				}
        // '|' pressed				
        if (keyCode == 124 && check_start()) {
          $('div.uauto-widget').html('<div id="uauto-k" class="uauto-i-block"><div class="uauto-i-list"><input name="cancel" type="button" value="Cancel" id="uauto-k-cancel" style="width:110px;"><div id="uauto-k-results" ></div><hr noshade></div><div id="uauto-i-macro" class="uauto-i-macro"><div id="uauto-k-message" class="uauto-i-message">uauto - Message </div></div></div>');
					if ($.browser.msie) {
					  $('#uauto-k').css("left",50).css("top",150).slideDown("slow");
					}
					else {
					  var y = (50 + 1*window.pageYOffset) + "px";
					  var x = (50 + 1*window.pageXOffset) + "px";
						$('#uauto-k').css("left",x).css("top",y).slideDown("slow");
					}
          $('#uauto-k-message').html("Listening..").slideDown("slow");
          
					// cancel button listener - window 1
          $('#uauto-k-cancel').click( function() {
            $('#uauto-k').slideUp("slow");
            mode = 0;
          }); 				
          mode = 1;          
        } 
        break;
        
      case 1:
        // '[' or ']' pressed
        if (keyCode == 93 || keyCode == 91) { 
          $('#uauto-k').hide("fast");
          mode = 0;
        }
				// '|' pressed, user will be given options for text part
        else if (keyCode == 124 ) {
          $('div.uauto-widget').html('<div id="uauto-k" class="uauto-i-block"><div class="uauto-i-list"><input name="cancel" type="button" value="Cancel" id="uauto-k-cancel" style="width:225px;"><div id="uauto-k-results" ><ul class="uauto"><li id="uauto-link-text">' + link_text + '</li></ul></div></div></div>');

					if ($.browser.msie) {
					  var y = window.screenTop*1+100;
					  $('#uauto-k').css("left",50).css("top",150).slideDown("slow");
					}
					else {
					  var y = (50 + 1*window.pageYOffset) + "px";
					  var x = (50 + 1*window.pageXOffset) + "px";
						$('#uauto-k').css("left",x).css("top",y).slideDown("slow");
					}					
					// apply link text
          $("#uauto-link-text").click(function () {
            if (body) {
              $('#uauto-k').hide("fast");
              insertText(get_cursorpos(), 0, link_text);
              mode = 0;
            }      
          });					
					// cancel button listener - window 2
          $('#uauto-k-cancel').click( function() {
            $('#uauto-k').hide("fast");
            mode = 0;
          });          
        }
				//trigger ksearch after delaying the search for 2 characters and k_timeout atleast          
        else if (lenOfId > 1 && ((keyCode >44 && keyCode< 123) || keyCode == 8)) { 
          clearTimeout(kTimer);           
          $('#uauto-k-message').html("Listening..");
          kTimer = setTimeout("ajax_ksearch()", uauto.k_timeout);             
        } 
        lenOfId++;                  
        break;
      } 
    }); 
    
    //isearch root function    
    $("#edit-body").mouseup(function (e) {
		  body = this;
		  if (uauto.i_enable == 0)
        return true; 
		  
			var text = selText(false, '');
			if (text.length > 2) {
				ajax_isearch(text, e.clientX, e.clientY);
			}
			// to close unneccessary windows
			else if(state) {
			  state = false;
				$('#uauto-i').hide("fast");
			}
    });  
		
					
 });
} 

function ajax_isearch(text, x, y) {  
  state = true;
	// to check, whether already the tag has been applied. if so allow user to edit the link  again
	var re = /\[l[^\|]*\|([^\|\]]+)\|?([^\|\]]*)\|?[^\]]*\]/;
	var m = re.exec(text);
	var tempText = '';
	if (m != null) {
		text = m[2];
		tempText = 'disabled';
	}	
	$('#uauto-widget').html('<div id="uauto-i" class="uauto-i-block"><div class="uauto-i-list"><input name="cancel" type="button" value="Cancel" id="uauto-i-cancel" style="width:110px;"><input name="clear" type="button" value="Clear cache" id="uauto-i-clear" style=" width:115px;"><div id="uauto-i-list" ></div><hr noshade> <input name="Apply all" id="uauto-i-apply" type="checkbox" value="Apply All" '+tempText+'> Apply to All </div><div id="uauto-i-macro" class="uauto-i-macro"><div id="uauto-i-message" class="uauto-i-message">uauto - Message </div><input name="macro" type="button" value="M" id="uauto-i-macro-s" style="float:right;width:20px;" '+tempText+'></div></div>');
	x = (1*y+20) + "px";
	y = (1*y+50) + "px";
	$('#uauto-i').css("left",x).css("top",y);
  $('#uauto-i').slideDown("slow");	
  $('#uauto-i-results').slideUp("fast");
  $('#uauto-i-message').html("Loading..").slideDown("slow");

  // cancel button listener - window 3
	$('#uauto-i-cancel').click( function() {
		$('#uauto-i').hide("fast");		
		state = false;
	});	
	// clearing cache - to save memory
	$('#uauto-i-clear').click( function() {
		if (confirm("Do you want to clear cache? ")) {
			cacheList = new Object();
			$('#uauto-i').hide("fast");		
			state = false;
		}
	});
	//check whether available in the cache	
	if  (cacheList[text])  {
		apply_isearch(cacheList[text], text, m);		
		$('#uauto-i-message').html("Locally Cached..");

	}
  //if not perform ajax search
	else {
	  $('#uauto-i-message').html("Connecting..");
		var link = uauto.href + "/isearch/" + text ;
	  var iSearch = function (data) {  
	    if (data) {
	      cacheList[text] =  Drupal.parseJson(data);
				apply_isearch(cacheList[text], text, m);
        $('#uauto-i-message').html("Loaded!");
			}			
	  }
	  $.get(link, null, iSearch); 
	}
	
}

function apply_isearch(result, text, m) {
  //populate the search results
	var htmlText = '';   
	var tempHtml = '';
	var apply = false;

	for (var i in result) {
		htmlText += "<li class=\"uauto\" id=\"" +  i + "\" title=" + i + " name=\"" + result[i] + "\" >" + result[i] + "</li>";
	}
	htmlText = htmlText ? '<ul class="uauto">' + htmlText + '</ul>' : "No results found";
	$('#uauto-i-list').html(htmlText).slideDown("slow");
   
  //macros -  display handling
 	$("#uauto-i-macro-s").click( function (){	
		var htmlText1 = '';
		for (var i in uauto.macro) {
			htmlText1 += "<li class=\"uauto-macro\" id=\"" +  i + "\" title=" + i + "><strong> " + i + "</strong> > " + uauto.macro[i] + "</li>";
		}
		htmlText1 = "<ul class=\"uauto\">" + htmlText1 + "</ul>";		
		tempHtml = $('#uauto-i-macro').html();
		$('#uauto-i-macro').html(htmlText1).slideDown("slow");
		$('#uauto-i-message').html("Macro loaded");
		
		//macros -  click handling
		$("ul > li.uauto-macro").click( function (){
			macro = this.id;    
			$('#uauto-i-macro').html(tempHtml);
		});  			
	});  
	// handling checkboxes
	$("input[@type=checkbox]").click( function() {	
		apply = (this.id=="uauto-i-apply") ? this.checked: apply;
	});	
	//apply search results	
	$('ul > li.uauto').click( function() {	
    var mac;	
		if (!macro)
			mac = 'l';
		else
			mac = 'l-' + macro;
			
		// apply all option
		if (apply) {
			re = new RegExp(text, "g");
			body.value = body.value.replace(re, "[" + mac + "|" + this.id + "|" + text + "]");	 
		}
		else {
		  // if it is an already applied tag, only replaces the link part
			if (m != null) {
				m[0] = m[0].replace(m[1], this.id);	
				selText(true, m[0]);
			}
			else {
				selText(true, "[" + mac + "|" + this.id + "|" + text + "]"); 
			}
		}
		$('#uauto-i').hide("fast");
		state = false;     
		
		//save the selected results in  the teble	
		if (applyCache) {
			var link = uauto.href + "/storeCache";		
			var storeCache = function (data) {  
				;
			}
			$.post(link, {uauto_text:text,uauto_key:this.id,uauto_desc:cacheList[text][this.id]}, storeCache);	
		}		
	});   
		
	
}

function ajax_ksearch() {
  $('#uauto-k-message').html("Loading..");
  cursorPos = get_cursorpos();
  var vid = get_buffer(body.value.substring(0, cursorPos), "|");
	
  var vid_len = vid.length;

	if  (cacheList[vid])  {
		$('#uauto-k-message').html("Locally Cached..");
    apply_ksearch(cacheList[vid], cursorPos, vid_len, vid)  ;
	}
	else {
    var vidlnk = uauto.href + "/ksearch/" + vid.replace('/','@$!');;
	  var voteSaved = function (data) {
			cacheList[vid] = Drupal.parseJson(data); 
      apply_ksearch(cacheList[vid], cursorPos, vid_len, vid)  ;
	  }
		$.get(vidlnk, null, voteSaved);
		$('#uauto-k-message').html("Loaded!");
  } 
}

function apply_ksearch(result, cursorPos, vid_len, vid) {
  var htmlText = '';   
	
	for (var i in result) {
		htmlText += "<li class=\"uauto\" id=\"" + i  + "\" title=" + i + " name=\"" + result[i] + "\" >" + result[i] + "</li>";
	}
	htmlText = htmlText ? '<ul class="uauto">' + htmlText + '</ul>' : "No results found";
	$('#uauto-k-results').html(htmlText).slideDown("slow");
			
	$("ul > li.uauto").click( function (){
		var text = this.id;
		link_text = this.firstChild.nodeValue;
		insertText(cursorPos, vid_len, text);
		$('#uauto-k').hide("fast");
		//save the selected results in  the teble	
    
		if (applyCache) {
			var link = uauto.href + "/storeCache";
			var storeCache = function (data) {  
			;
			}
			$.post(link, {uauto_text:vid,uauto_key:this.id,uauto_desc:cacheList[vid][this.id]}, storeCache);	
		}
	});		
}

//may be needed in several places in future :-)
function get_buffer(text, delim) {
  return text.slice(text.lastIndexOf(delim)+1, text.length);
}  

//check whether to start auto completion
function check_start() {
  var pos = get_cursorpos();
	var keyword = "[l";
	
	if (body.value.substring(pos-keyword.length,pos) == keyword) 
    return true;
  // to allow macros
	for (var i in uauto.macro) {
	  keyword = "[l-" + i;
	  if (body.value.substring(pos-keyword.length,pos) == keyword)
			return true;
	}
  return false;
}

// insert text at the cursor position
function insertText(cursorPos, vid_len, text) {
  if ($.browser.safari || $.browser.opera || $.browser.mozilla) {
    var oldStringFirst = body.value.substring(0,cursorPos+1);
    var oldStringLast =  body.value.substring(cursorPos);    
    oldStringFirst =  body.value.substr(0,oldStringFirst.lastIndexOf('|')+1);      
    body.value =   oldStringFirst + text + oldStringLast;
    var len = body.value.length - oldStringLast.length;
    body.focus();
    body.setSelectionRange(len,len);
  }
  else if ($.browser.msie){
    body.focus();
    var caretPos = document.selection.createRange();
    caretPos.moveStart("character",-1*(vid_len));
    caretPos.select();
    caretPos.text = text;
    caretPos.select();       
  }    
}

//cursorpos currently depends on the browser, getting the cursor position
function get_cursorpos() {
  body.focus(); 

  if ($.browser.safari || $.browser.opera || $.browser.mozilla) {  
    if (typeof body.setSelectionRange != 'undefined') {
      return body.selectionStart;      
    }     
  }
 else if($.browser.msie) {   //IE doesn't have a direct method to get the cursor position
    if ( body.isTextEdit ) { 
      body.caretPos = document.selection.createRange(); 
    }
    body.caretPos.text = '#@#@#'; // a unique string
    var pos = body.value.indexOf('#@#@#', 0);
    body.caretPos.select();
    body.caretPos.moveStart("character",-5);
    body.caretPos.select();
    body.caretPos.text = '';    
    return pos;
  }
  else {
    return length(body.value);    
  } 
}

// get the user selected text or replaces the text with 'text'
function selText(set, text) { 
  body.focus();
	
	if (set) {
	  if (typeof body.setSelectionRange != 'undefined') {
			body.value = body.value.substring(0,body.selectionStart) + text +  body.value.substring(body.selectionEnd);
			body.focus();
			body.setSelectionRange(selectionStart,selectionStart+text.length);
		}
		else {
		  userSelection.text = text;
			userSelection.select();    
		}
	}
	else {
	  if (typeof body.setSelectionRange != 'undefined') {
		  selectionStart = body.selectionStart;
			selectionEnd = body.selectionEnd;
			return body.value.substring(body.selectionStart, body.selectionEnd);      
		} 
		else if (document.selection) { // should come last; Opera!
			var selectedText = userSelection = document.selection.createRange();
			if (userSelection.text)
				selectedText = userSelection.text;
			return selectedText;
		}	  
	}	
}