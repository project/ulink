<?php
/**
 * Implementation of hook_ulink_image()
 */
function ulink_ulink_image($link, $object=NULL){
  switch (variable_get('ulink_image_settings', '0')) {
    case '0':
    // having alt and theme will have an effect of having double alt, title tags (eg: alt given directly and alt from atttributes but for compatibility it is left as repeating tags don't do any harm
	  return theme('image', $link['path'], $link['attributes']['alt'], $link['attributes']['title'], $link['attributes'], FALSE);	
	case '1':  
	  $text = $link['text'] ? $link['text'] : $link['path'];
	  return l($text, $link['path'], $link['attributes'], NULL, NULL, FALSE, TRUE);
  case '2':
	  $image = theme('image', $link['path'], $link['attributes']['alt'], $link['attributes']['title'], $link['attributes'], FALSE);
    return l($image, file_create_url($link['path']), $link['attributes'], NULL, NULL, FALSE, TRUE);
  }  	
}
/**
 * Implementation of hook_ulink_image_settings()
 */
function ulink_ulink_image_settings(){
  $options = array('0' => t('Only image'), '1' =>t('Only link'), '2' => t('Image and link'));
  $form['ulink_image_settings'] = array(
    '#title' => t('Redering option'),    
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => variable_get('ulink_image_settings', '0'),  
    '#description' => (module_exists('imagecache')&&module_exists('ulink_imagecache') ? t('Please use <a href="!settings">ulink_imagecache</a> implementation for images. For specific rendering, please remove the extension from the list and add it in the others/files category with implementation.', array('!settings' => url('admin/settings/ulink/image'))) : t('<strong>Note:</strong> If <a href="!imagecache">Imagecache</a> module and  ulink_imagecache are installed, uLink provides support for image scaling. For specific rendering, please remove the extension from the list and add it in the others/files category with implementation.', array('!imagecache' => url('http://drupal.org/project/imagecache')))),
  );
	return $form;
}
/**
 * Implementation of hook_ulink_image_info().
 */
function ulink_ulink_image_info() {
  return t(' If imagecache is available, please use ulink_imagecache implementation - it is used for images from "files/". Otherwise HTML image tag is rendered. User can override this by specifying force_link=true : attach the imagefile instead displaying it.');
}
