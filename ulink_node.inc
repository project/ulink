<?php
/**
 * Implementation of hook_ulink_node()
 */
function ulink_ulink_node($link, $node=NULL){
  return _ulink_ulink_xxx('node', $link, $node);
}
/**
 * Implementation of hook_ulink_node_settings()
 */
function ulink_ulink_node_settings() {
  return _ulink_ulink_settings_builder('node');
}
/**
 * Implementation of hook_ulink_node_info()
 */
function ulink_ulink_node_info() {
  return t(' Three modes of rendering are possible. Hardcoded rendering for novice users and PHPcode for advanced users. In the macro mode TOKENS can be used. Use configurations to individually configure them to match the requirement.');
}
/*
 * support functions to provide extra settings pages.
 */
function _ulink_node_settings_1() {
  return system_settings_form(_ulink_xxx_settings_1('node'));
} 
function _ulink_node_settings_2() {
  return system_settings_form(_ulink_xxx_settings_2('node'));
}
function _ulink_node_settings_3(){
  return system_settings_form(_ulink_xxx_settings_3('node'));
}
