<?php
/**
 * Implementation of hook_ulink_others()
 */
function ulink_ulink_others($link, $object=NULL){
  $path = $link['path'];
  $text = $link['text'];
  $attributes = $link['attributes'];
  switch (variable_get('ulink_others_settings_option', 1)) {
    case 1:
      $text = $text ? $text : $path;
      return l($text, $path, $attributes);
    case 2:
      if (module_exists('token')) {
        return token_replace(variable_get('ulink_others_2_tag', '<a href="[ulink-path]" [ulink-attributes-string]>[ulink-text]</a>'), $type = 'ulink', $link);
      }
      else {
        return $text;
      }	  
	case 3:
	  return eval(variable_get('ulink_others_3_code', 'return l($text." ( external file )", $path, $link["attributes"]);'));
  }
}
/**
 * Implementation of hook_ulink_settings()
 */
function ulink_ulink_others_settings() {
  return _ulink_ulink_settings_builder('others');
}

/**
 * Implementation of hook_ulink_info()
 */
function ulink_ulink_others_info() {
  return t(' Three modes of rendering are possible. Hardcoded rendering for novice users and PHPcode for advanced users. In the macro mode TOKENS can be used. Use configurations to individually configure them to match the requirement.');
}


/*
 * support functions to provide extra settings pages.
 */
function _ulink_others_settings_2() {
  return system_settings_form(_ulink_xxx_settings_2('others'));
}

function _ulink_others_settings_3() {
 return  system_settings_form(_ulink_xxx_settings_3('others'));
}
