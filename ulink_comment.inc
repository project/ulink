<?php
/**
 * Implementation of hook_ulink_comment().
 */
function ulink_ulink_comment($link, $comment=NULL){
  return _ulink_ulink_xxx('comment', $link, $comment);
}
/**
 * Implementation of hook_ulink_comment_settings().
 */
function ulink_ulink_comment_settings() {
  return _ulink_ulink_settings_builder('comment');
}
/**
 * Implementation of hook_ulink_comment_info().
 */
function ulink_ulink_comment_info() {
  return t(' Three modes of rendering are possible. Hardcoded rendering for novice users and PHPcode for advanced users. In the macro mode TOKENS can be used. Use configurations to individually configure them to match the requirement.');
}
/*
 * support functions to provide extra settings pages.
 */
function _ulink_comment_settings_1() {
  return system_settings_form(_ulink_xxx_settings_1('comment'));
} 
function _ulink_comment_settings_2() {
  return system_settings_form(_ulink_xxx_settings_2('comment'));
}
function _ulink_comment_settings_3() {
  return system_settings_form(_ulink_xxx_settings_3('comment'));
}